package lab1_2;

import java.util.Arrays;
import java.util.Comparator;

public class ResearchTeam {
	private String themeName;
	private String organizationName;
	private int registerNumber;
	private TimeFrame time;
	private Paper[] listPublication;

	public ResearchTeam(String themeName, String organizationName,
			int registerNumber, TimeFrame time) {
		this.setThemeName(themeName);
		this.setOrganizationName(organizationName);
		this.setRegisterNumber(registerNumber);
		this.setTime(time);
	}

	public ResearchTeam() {
		this("Project X", "YobaStyle", 10122, TimeFrame.Year);
	}

	public String getThemeName() {
		return themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}

	public String getOrganizationName() {
		return organizationName;
	}

	public void setOrganizationName(String organizationName) {
		this.organizationName = organizationName;
	}

	public int getRegisterNumber() {
		return registerNumber;
	}

	public void setRegisterNumber(int registerNumber) {
		this.registerNumber = registerNumber;
	}

	public TimeFrame getTime() {
		return time;
	}

	public void setTime(TimeFrame time) {
		this.time = time;
	}

	public Paper[] getListPublication() {
		return listPublication;
	}

	public void setListPublication(Paper[] listPublication) {
		this.listPublication = listPublication;
	}

	public Paper getPaper() {
		if (listPublication == null || listPublication.length == 0) {
			return null;
		}
		Arrays.sort(listPublication, new Comparator<Paper>() {

			@Override
			public int compare(Paper paper1, Paper paper2) {
				// TODO Auto-generated method stub
				return paper1.getDate().compareTo(paper2.getDate());
			}
		});
		return listPublication[listPublication.length - 1];
	}

	public boolean indexator(TimeFrame time) {
		return this.getTime() == time;
	}

	public void addPapers(Paper[] list) {
		if (listPublication == null) {
			this.setListPublication(list);
		} else {
			Paper[] temp = new Paper[list.length + listPublication.length];
			for (int i = 0; i < listPublication.length; i++) {
				temp[i] = listPublication[i];
			}
			int currentCount = listPublication.length;
			for (int i = 0; i < list.length; i++) {
				temp[currentCount++] = list[i];
			}
			this.setListPublication(temp);
		}
	}

	@Override
	public String toString() {
		return "Theme Publication: " + getThemeName()
				+ "\nOrgranization Name: " + getOrganizationName()
				+ "\nRegister Number: " + getRegisterNumber()
				+ "\nList Publication: " + join(",", getListPublication())
				+ "\nTime Frame: " + getTime();
	}

	public String toShortString() {
		return "Theme Publication: " + getThemeName()
				+ "\nOrgranization Name: " + getOrganizationName()
				+ "\nRegister Number: " + getRegisterNumber()
				+ "\nTime Frame: " + getTime();
	}

	protected String join(String delimeter, Object[] array) {
		String result = "";
		for (int i = 0; i < array.length; i++) {
			if (i == array.length - 1) {
				result += array[i];
			} else {
				result += array[i] + delimeter + " ";
			}
		}
		return result;
	}
}
