package lab1_2;

import lab1_1.Person;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ResearchTeam team = new ResearchTeam();
		System.out.println(team.toShortString());
		System.out.println("Year: " + team.indexator(TimeFrame.Year));
		System.out.println("TwoYears: " + team.indexator(TimeFrame.TwoYears));
		System.out.println("Long: " + team.indexator(TimeFrame.Long));
		team.setThemeName("LAB1");
		team.setOrganizationName("IIT BSUIR");
		team.setListPublication(new Paper[]{
				new Paper("Lab1 Ez", new Person("Alex", "Miklashevich", "14.06.1997"),"25.08.2016"),
				new Paper("True Creater", new Person("Vladislav", "Belikov", "28.05.1997"),"09.09.2017")
				});
		team.setTime(TimeFrame.Long);
		team.addPapers(new Paper[]{
				new Paper("Levyi Chel", new Person("Nikolay", "Bobrov", "01.08.1997"), "21.08.2016"),
				new Paper("QA worker", new Person("Alexandr", "Tihonovich", "27.12.1996"),"18.08.2017")
		});
		System.out.println(team.getPaper());
		System.out.println(team);
		

	}

}
