package lab1_1;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Person {
	private String name;
	private String lastName;
	private Date date;
	private SimpleDateFormat format;

	public Person(String name, String lastName, String date) {
		format = new SimpleDateFormat("dd.MM.yyyy");
		this.setName(name);
		this.setLastName(lastName);
		this.setDate(date);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDate() {
		return date;
	}

	protected boolean setDate(String date) {

		try {
			this.date = format.parse(date);
			return true;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
	}

	public void setDate(int year) {
		date.setYear(year);
	}

	@Override
	public String toString() {
		return getName() + " " + getLastName() + " " + format.format(getDate());
	}

	public String toShortString() {
		return getName() + " " + getLastName();
	}
}
