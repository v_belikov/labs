package lab4_1;

import lab2_1.Person;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.ListIterator;

public class ResearchTeam extends Team implements Comparator<ResearchTeam> {
	private String themeName;
	private TimeFrame time;
	private ArrayList<Person> persons;
	private ArrayList<Paper> listPublication;
	private Team team;

	public ResearchTeam(String themeName, String organizationName,
			int registerNumber, TimeFrame time) throws Exception {
		super(organizationName, registerNumber);
		this.setThemeName(themeName);
		this.setTime(time);
		this.setTeam(this);
		listPublication = new ArrayList<>();
		persons = new ArrayList<>();
	}

	public ResearchTeam() throws Exception {
		this("Project X", "YobaStyle", 10122, TimeFrame.Year);
	}

	public int getCountPublication() {
		return listPublication.size();
	}

	public int getCountMembers() {
		return persons.size();
	}

	public void addMembers(ArrayList<Person> members) {
		persons.addAll(members);
	}

	public String getThemeName() {
		return themeName;
	}

	public void setThemeName(String themeName) {
		this.themeName = themeName;
	}

	public TimeFrame getTime() {
		return time;
	}

	public void setTime(TimeFrame time) {
		this.time = time;
	}

	public ArrayList<Paper> getListPublication() {
		return listPublication;
	}

	public Paper getPaper() {
		if (listPublication == null || listPublication.size() == 0) {
			return null;
		}
		listPublication.sort(new Comparator<Paper>() {

			@Override
			public int compare(Paper paper1, Paper paper2) {
				// TODO Auto-generated method stub
				return paper1.getDate().compareTo(paper2.getDate());
			}
		});
		return listPublication.get(listPublication.size() - 1);
	}

	public boolean indexator(TimeFrame time) {
		return this.getTime() == time;
	}

	public void addPapers(ArrayList<Paper> list) {
		this.getListPublication().addAll(list);
	}

	@Override
	public String toString() {
		return "Theme Publication: " + getThemeName() + "\n" + super.toString()
				+ "\nList Publication: " + join(",", getListPublication())
				+ "\nTime Frame: " + getTime() + "\nMembers: "
				+ join(",", persons);
	}

	public String toShortString() {
		return "Theme Publication: " + getThemeName() + "\n" + super.toString()
				+ "\nTime Frame: " + getTime() + "\nMembers: "
				+ join(",", persons);
	}

	protected String join(String delimeter, ArrayList<?> array) {
		String result = "";
		for (int i = 0; i < array.size(); i++) {
			if (i == array.size() - 1) {
				result += array.get(i);
			} else {
				result += array.get(i) + delimeter + " ";
			}
		}
		return result;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
		this.registerNumber = team.registerNumber;
		this.name = team.name;
	}

	public ListIterator<Person> getMembers() {
		ArrayList<Person> list = new ArrayList<>();
		for (Person person : persons) {
			boolean publicate = false;
			for (Paper paper : listPublication) {
				if (person.equals(paper.getPerson())) {
					publicate = true;
				}
			}
			if (!publicate) {
				list.add(person);
			}
		}
		return list.listIterator();

	}

	public ListIterator<Paper> getPublications(int countYear) {
		ArrayList<Paper> list = new ArrayList<>();
		int currentYear = new Date().getYear();
		for (Paper paper : listPublication) {
			if (paper.getDate().getYear() >= currentYear - countYear) {
				list.add(paper);
			}
		}
		return list.listIterator();
	}

	@Override
	public Object deepCopy() {
		ResearchTeam resTeam = null;
		try {
			resTeam = new ResearchTeam(new String(getThemeName()), new String(
					getName()), registerNumber, time);
			resTeam.addMembers((ArrayList<Person>) persons.clone());
			resTeam.addPapers((ArrayList<Paper>) listPublication.clone());
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return resTeam;
	}

	@Override
	public int compare(ResearchTeam researchTeam1, ResearchTeam researchTeam2) {
		// TODO Auto-generated method stub

		return researchTeam1.getThemeName().compareTo(
				researchTeam2.getThemeName());
	}

	public void sortByPaperName() throws ParseException {
		listPublication.sort(new Paper());
	}

	public void sortByPaperDate(){
		listPublication.sort(new Comparator<Paper>() {
			@Override
			public int compare(Paper paper1, Paper paper2) {
				return paper1.compareTo(paper2);
			}
		});
	}

	public void sortByPersonLastName(){
		listPublication.sort(new ComparePaperByPersonName());
	}

}
