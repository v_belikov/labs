package lab4_1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class TestCollections {
	protected ArrayList<Team> keyCollection;
	protected ArrayList<String> stringCollection;
	protected HashMap<Team, ResearchTeam> keyDictionary;
	protected HashMap<String, ResearchTeam> stringDictionary;

	public static ResearchTeam getResearchTeam(int number) {
		ResearchTeam team = null;
		int time = number % 3;
		try {
			team = new ResearchTeam("Theme Name " + number,
					"Organization Name " + number, number,
					time == 0 ? TimeFrame.Year : time == 1 ? TimeFrame.TwoYears
							: TimeFrame.Long);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return team;
	}

	public TestCollections(int count) {
		keyCollection = new ArrayList<>();
		stringCollection = new ArrayList<String>();
		keyDictionary = new HashMap<>();
		stringDictionary = new HashMap<>();
		for (int i = 0; i < count; i++) {
			try {
				keyCollection.add(new Team("Theme Name " + i, i + 1));
				stringCollection.add("String Collection " + i + 1);
				keyDictionary.put(new Team("Theme Name" + i, i + 1),
						TestCollections.getResearchTeam(i + 1));
				stringDictionary.put("String Collection" + i,
						TestCollections.getResearchTeam(i + 1));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void testTime() {
		String title = "============%s============";
		System.out.println(String.format(title, "List Team"));

		long currentTime = System.currentTimeMillis();
		for (int i = 0; i < keyCollection.size(); i++) {
			keyCollection.get(i);
			if(i == 0 || i == keyCollection.size() / 2 || i == keyCollection.size() - 1)
			System.out.println((i + 1) + ": "
					+ (System.currentTimeMillis() - currentTime));
		}

		System.out.println(String.format(title, "List String"));

		currentTime = System.currentTimeMillis();
		for (int i = 0; i < stringCollection.size(); i++) {
			stringCollection.get(i);
			if(i == 0 || i == stringCollection.size() / 2 || i == stringCollection.size() - 1)
			System.out.println((i + 1) + ": "
					+ (System.currentTimeMillis() - currentTime));
		}
		System.out.println(String.format(title, "Key Team Dictionary"));
		int i = 0;
		currentTime = System.currentTimeMillis();
		for (Entry<Team, ResearchTeam> entry : keyDictionary.entrySet()) {
			Team key = entry.getKey();
			keyDictionary.get(key);
			if(i == 0 || i == keyDictionary.size() / 2 || i == keyDictionary.size() - 1)
			System.out.println((i + 1) + ": "
					+ (System.currentTimeMillis() - currentTime));
			i++;
		}

		System.out.println(String.format(title, "Key String Dictionary"));
		i = 0;
		currentTime = System.currentTimeMillis();
		for (Entry<String, ResearchTeam> entry : stringDictionary.entrySet()) {
			String key = entry.getKey();
			stringDictionary.get(key);
			if(i == 0 || i == stringDictionary.size() / 2 || i == stringDictionary.size() - 1)
			System.out.println((i + 1) + ": "
					+ (System.currentTimeMillis() - currentTime));
			i++;
		}
	}
}
