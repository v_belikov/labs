package lab4_1;


public class Main {

    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
    	ResearchTeamCollection<String> rtc1 = new ResearchTeamCollection<>("firstCollection");
    	ResearchTeamCollection<String> rtc2 = new ResearchTeamCollection<>("secondCollection");
    	rtc1.addDefaults();
    	rtc2.addDefaults();

    	rtc1.insertAt(12, new ResearchTeam());
    	rtc2.insertAt(2, new ResearchTeam());
    	EventManager.getInstance().printLog();
    }

}
