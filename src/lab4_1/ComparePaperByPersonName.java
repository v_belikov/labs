package lab4_1;

import java.util.Comparator;

public class ComparePaperByPersonName implements Comparator<Paper>{
    @Override
    public int compare(Paper paper1, Paper paper2) {
        return paper1.getPerson().getLastName().compareTo(paper2.getPerson().getLastName());
    }
}
