package lab4_1;

public interface INameAndCopy {
	String getName();
	void setName(String name);
	Object deepCopy();
}
