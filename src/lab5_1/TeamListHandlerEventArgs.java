package lab5_1;

public class TeamListHandlerEventArgs {
	private String collectionName;
	private String type;
	private Integer index;

	public TeamListHandlerEventArgs(String collectionName, String type,
			Integer index) {
		this.collectionName = collectionName;
		this.type = type;
		this.index = index;
	}

	public String getCollectionName() {
		return collectionName;
	}

	public String getType() {
		return type;
	}

	public Integer getIndex() {
		return index;
	}

	public String toString() {
		return String.format(
				"Collection Name: %s\nType Event: %s\nIndex item: %s",
				this.getCollectionName(), this.getType(), this.getIndex());
	}
}
