package lab5_1;

import java.io.Serializable;

public enum TimeFrame implements Serializable {
	Year, TwoYears, Long
}
