package lab5_1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public class EventManager {
	private HashMap<String, ArrayList<TeamListHandlerEventArgs>> events; // массив с названиями событий и сами события
	private static EventManager eventManager;

	public static EventManager getInstance() {
		return eventManager == null ? eventManager = new EventManager()
				: eventManager;
	}

	private EventManager() {
		events = new HashMap<>();
	}

	public void call(TeamListHandlerEventArgs event) throws Exception {
		if (events.get(event.getType()) == null) {
			throw new Exception("Unregistred event");
		}
		events.get(event.getType()).add(event); // получаем список событий по названию, и добавляем новое событие в конец

	};

	public void addEventListener(String type) {
		if (events.get(type) == null) {
			events.put(type, new ArrayList<TeamListHandlerEventArgs>());
		}
	}

	public void removeEventListener(String type) {
		events.remove(type);
	}
	public void printLog() { // какие типы событий были
		for(Entry<String, ArrayList<TeamListHandlerEventArgs>> entry : events.entrySet()) {
		    String key = entry.getKey();
		    this.printLog(key);
		}
	}
	public void printLog(String type) { // выводим события конкретного типа
		ArrayList<TeamListHandlerEventArgs> list = events.get(type);
		System.out.println("Events type '" + type + "':");
		for (TeamListHandlerEventArgs event : list) {
			System.out.println(event);
		}
	}
	
	public void printLog(String type, String collectionName) {
		ArrayList<TeamListHandlerEventArgs> list = events.get(type);
		System.out.println("Events type '" + type + "':");
		for (TeamListHandlerEventArgs event : list) {
			if(event.getCollectionName().equals(collectionName)) {
				System.out.println(event);
			}
		}
	}
}
