package lab5_1;


public class Main {

    public static void main(String[] args) throws Exception {
        // TODO Auto-generated method stub
		ResearchTeam team1 = new ResearchTeam();
		while (team1.addFromConsole());
		ResearchTeam.save("FileSaver.txt", team1);

        ResearchTeam team2 = new ResearchTeam("Niopolizm", "GeoGlucci", 235435, TimeFrame.Long);
        System.out.println(team2);
        ResearchTeam.load("FileSaver.txt", team2);
        System.out.println(team2);

    }

}
