package lab3_2;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

public class ResearchTeamCollection<TKey> {
	private ArrayList<ResearchTeam> resList;
	private HashMap<TKey, ResearchTeam> collection;
	private int countDefault;
	private KeySelector<TKey> selector;
	public ResearchTeamCollection() {
		this.resList = new ArrayList<ResearchTeam>();
		this.collection = new HashMap<>();
		this.selector = new KeySelector<TKey>();
		this.countDefault = 5;
	}
	public ResearchTeamCollection(KeySelector<TKey> selector) {
		this();
		this.selector = selector;
	}
	public void addDefaults() {

		for (int i = 0; i < countDefault; i++) {
			try {
				ResearchTeam researchTeam = new ResearchTeam();
				collection.put(selector.generate(researchTeam), researchTeam);
				resList.add(researchTeam);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public Paper LastPublicationPaper() throws ParseException {
		ArrayList<Paper> papers = new ArrayList<>();
		for (ResearchTeam item : collection.values()){
			Paper paper = item.getPaper();
			if(paper != null) {
				papers.add(paper);
			}
		}
		if(papers.size() == 0) {
			return null;
		}
		papers.sort(new Paper());
		return papers.get(papers.size() - 1);
	}

	public ArrayList<ResearchTeam> timeFrameGroup(TimeFrame timeFrame){
		ArrayList<ResearchTeam> researchTeams = new ArrayList<>();
		for (ResearchTeam researchTeam : collection.values()){
			if (researchTeam.getTime() == timeFrame){
				researchTeams.add(researchTeam);
			}
		}
		return researchTeams;
	}

	public ArrayList<ArrayList<ResearchTeam>> timeFramGroups(){
		ArrayList<ArrayList<ResearchTeam>> result = new ArrayList<>();
		result.add(timeFrameGroup(TimeFrame.Year));
		result.add(timeFrameGroup(TimeFrame.TwoYears));
		result.add(timeFrameGroup(TimeFrame.Long));
		return result;

	}

	public void addResearchTeams(ArrayList<ResearchTeam> team) {
		for (ResearchTeam item : team) {
			collection.put(selector.generate(item), item);
			resList.add(item);
		}
	}

	public void addResearchTeams(ResearchTeam[] team) {
		for (ResearchTeam item : team) {
			collection.put(selector.generate(item), item);
			resList.add(item);
		}
	}

	@Override
	public String toString() {
		String result = "";
		for (ResearchTeam item : resList) {
			result += item + "\n";
		}
		return result;
	}

	public String toShortString() {
		String result = "";
		for (ResearchTeam item : resList) {
			result += String.format("\nTheme Name: %s" + "\nTime Frame: %s"
					+ "\nCount Persons: %s" + "\n Count Publication: %s"
					+ "\n Team Information: %s", item.getThemeName(),
					item.getTime(), item.getCountMembers(),
					item.getCountPublication(), item.getTeam());
		}
		return result;
	}

	public void sortByThemeName() {
		try {
			resList.sort(new ResearchTeam());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sortByCountPublication() {
		resList.sort(new CompareResearchTeamByCountPublication());
	}

	public void sortByRegisterNumber() {
		resList.sort(new Comparator<ResearchTeam>() {

			@Override
			public int compare(ResearchTeam team1, ResearchTeam team2) {
				// TODO Auto-generated method stub
				return team1.compareTo(team2);
			}
		});
	}

	public int getMinRegisterNumber() {
		int min = -1;
		for (ResearchTeam resTeam : resList) {
			if (min == -1) {
				min = resTeam.getRegisterNumber();
			}
			min = Math.min(resTeam.getRegisterNumber(), min);
		}
		return min;
	}

	public ArrayList<ResearchTeam> getTwoYearsItems() {
		ArrayList<ResearchTeam> items = new ArrayList<>();
		for (ResearchTeam item : resList) {
			if (item.getTime() == TimeFrame.TwoYears) {
				items.add(item);
			}
		}
		return items;
	}

	public ArrayList<ResearchTeam> nGroup(int countMembers) {
		ArrayList<ResearchTeam> items = new ArrayList<>();
		for (ResearchTeam item : resList) {
			if (item.getCountMembers() == countMembers) {
				items.add(item);
			}
		}
		return items;
	}

	private class KeySelector<TKey> {
		public TKey generate(ResearchTeam team){

			return  (TKey)team;//Потом подумаем как
		}
	}

}
