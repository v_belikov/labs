package lab3_2;

public class Team implements INameAndCopy, Comparable<Team> {
	protected String name;
	protected int registerNumber;

	public Team(String name, int registerNumber) throws Exception {
		this.setName(name);
		this.setRegisterNumber(registerNumber);
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		this.name = name;
	}

	@Override
	public Object deepCopy() {
		// TODO Auto-generated method stub
		Team object = null;
		try {
			object = new Team(new String(getName()), getRegisterNumber());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return object;
	}

	@Override
	public boolean equals(Object object) {
		Team team = (Team) object;
		return team.getName().equals(getName())
				&& team.getRegisterNumber() == getRegisterNumber();
	}

	@Override
	public int hashCode() {
		int hash = 0;
		for (int i = 0; i < name.length(); hash += name.charAt(i++))
			;
		hash += getRegisterNumber();
		return hash;
	}

	@Override
	public String toString() {
		return "Organization Name: " + getName() + "\nRegister Number: "
				+ getRegisterNumber();
	}

	public int getRegisterNumber() {
		return registerNumber;
	}

	public void setRegisterNumber(int registerNumber) throws Exception {
		if (registerNumber <= 0)
			throw new Exception("Negative number");
		this.registerNumber = registerNumber;
	}

	@Override
	public int compareTo(Team team) {
		if (this.getRegisterNumber() > team.getRegisterNumber())
			return 1;
		else if (this.getRegisterNumber() < team.getRegisterNumber())
			return -1;
		return 0;
	}
}
