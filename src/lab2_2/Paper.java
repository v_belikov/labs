package lab2_2;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import lab2_1.Person;

public class Paper {
	private String name;
	private Person person;
	private Date datePublication;
	private SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Date getDate() {
		return datePublication;
	}

	public void setDate(String date) {
		try {
			this.datePublication = format.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Paper(String name, Person person, String datePublication) {
		format = new SimpleDateFormat("dd.MM.yyyy");
		this.setName(name);
		this.setPerson(person);
		this.setDate(datePublication);
	}

	public Paper() throws ParseException {
		this("default", new Person("Name", "Last Name", "26.08.1989"),
				"23.01.2016");
	}

	@Override
	public String toString() {
		return getName() + " " + getPerson() + " " + format.format(getDate());
	}
}
