package lab2_2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import lab2_1.Person;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Team team1 = new Team("Team", 1122);
			Team team2 = new Team("Team", 1122);

			System.out.println("team1 equals team2: " + team1.equals(team2));
			System.out.println("team1 == team2: " + (team1 == team2));
			System.out.println("team1 hash: " + team1.hashCode());
			System.out.println("team2 hash: " + team2.hashCode());
			team1.setRegisterNumber(-12);

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		try {
			ResearchTeam resTeam1 = new ResearchTeam();
			Person vlad = new Person("Vladislav", "Belikov", "28.05.1997");
			Person alex = new Person("Alexey", "Miklashevich", "14.06.1997");
			Person alexandr = new Person("Alexandr", "Tihonovich", "27.12.1996");
			Person nikolay = new Person("Nikolay", "Bobrov", "01.08.1997");
			ArrayList<Person> members = new ArrayList<>();
			members.add(vlad);
			members.add(alex);
			members.add(alexandr);
			members.add(nikolay);
			ArrayList<Paper> listPub = new ArrayList<>();
			listPub.add(new Paper("Ez lab 2", alex, "12.08.2017"));
			listPub.add(new Paper("QA tester", alexandr, "1.12.2015"));
			resTeam1.addMembers(members);
			resTeam1.addPapers(listPub);
			ResearchTeam resTeam2 = (ResearchTeam) resTeam1.deepCopy();
			resTeam2.setName("My Team");
			resTeam2.setTime(TimeFrame.TwoYears);
			System.out.println("======================================");
			System.out.println(resTeam1);
			System.out.println("======================================");
			System.out.println(resTeam2);
			System.out.println("======================================");
			ListIterator<Paper> papers = resTeam1.getPublications(2);
			for (; papers.hasNext();) {
				System.out.println(papers.next());
			}
			System.out.println("======================================");
			ListIterator<Person> persons = resTeam1.getMembers();
			for (; persons.hasNext();) {
				System.out.println(persons.next());
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
