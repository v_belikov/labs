package lab2_2;

public interface INameAndCopy {
	String getName();
	void setName(String name);
	Object deepCopy();
}
