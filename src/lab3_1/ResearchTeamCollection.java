package lab3_1;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.function.Function;
import java.util.function.ToDoubleFunction;
import java.util.function.ToIntFunction;
import java.util.function.ToLongFunction;

public class ResearchTeamCollection {
	private ArrayList<ResearchTeam> resList;
	private int countDefault;
	public ResearchTeamCollection() {
		this.resList = new ArrayList<ResearchTeam>();
		this.countDefault = 5;
	}
	public void addDefaults() {
		if (resList == null) {
			resList = new ArrayList<>();
		}
		for (int i = 0; i < countDefault; i++) {
			try {
				resList.add(new ResearchTeam());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void addResearchTeams(ArrayList<ResearchTeam> team) {
		resList.addAll(team);
	}

	public void addResearchTeams(ResearchTeam[] team) {
		for (ResearchTeam item : team) {
			resList.add(item);
		}
	}

	@Override
	public String toString() {
		String result = "";
		for (ResearchTeam item : resList) {
			result += item + "\n";
		}
		return result;
	}

	public String toShortString() {
		String result = "";
		for (ResearchTeam item : resList) {
			result += String.format("\nTheme Name: %s" + "\nTime Frame: %s"
					+ "\nCount Persons: %s" + "\n Count Publication: %s"
					+ "\n Team Information: %s", item.getThemeName(),
					item.getTime(), item.getCountMembers(),
					item.getCountPublication(), item.getTeam());
		}
		return result;
	}

	public void sortByThemeName() {
		try {
			resList.sort(new ResearchTeam());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void sortByCountPublication() {
		resList.sort(new CompareResearchTeamByCountPublication());
	}

	public void sortByRegisterNumber() {
		resList.sort(new Comparator<ResearchTeam>() {

			@Override
			public int compare(ResearchTeam team1, ResearchTeam team2) {
				// TODO Auto-generated method stub
				return team1.compareTo(team2);
			}
		});
	}

	public int getMinRegisterNumber() {
		int min = -1;
		for (ResearchTeam resTeam : resList) {
			if (min == -1) {
				min = resTeam.getRegisterNumber();
			}
			min = Math.min(resTeam.getRegisterNumber(), min);
		}
		return min;
	}

	public ArrayList<ResearchTeam> getTwoYearsItems() {
		ArrayList<ResearchTeam> items = new ArrayList<>();
		for (ResearchTeam item : resList) {
			if (item.getTime() == TimeFrame.TwoYears) {
				items.add(item);
			}
		}
		return items;
	}

	public ArrayList<ResearchTeam> nGroup(int countMembers) {
		ArrayList<ResearchTeam> items = new ArrayList<>();
		for (ResearchTeam item : resList) {
			if (item.getCountMembers() == countMembers) {
				items.add(item);
			}
		}
		return items;
	}
}
