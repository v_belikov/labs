package lab3_1;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

import lab2_1.Person;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        // TODO Auto-generated method stub
        ResearchTeamCollection teamCollection = new ResearchTeamCollection();
        ArrayList<ResearchTeam> team = new ArrayList<ResearchTeam>();
        for (int i = 0; i < 10; i++) {
            ResearchTeam item = TestCollections.getResearchTeam(i + 1);
            team.add(item);
        }
        teamCollection.addResearchTeams(team);
        System.out.println(teamCollection);
        teamCollection.sortByThemeName();
        System.out
                .println("==================Sort By Theme Name==========================");
        System.out.println(teamCollection);
        teamCollection.sortByCountPublication();
        System.out
                .println("==================Sort By Count Publication==========================");
        System.out.println(teamCollection);
        teamCollection.sortByRegisterNumber();
        System.out
                .println("==================Sort By Register Number==========================");
        System.out.println(teamCollection);
        System.out.println("==================Min Register Number========================== "
                + teamCollection.getMinRegisterNumber());
        ResearchTeamCollection filter = new ResearchTeamCollection();
        filter.addResearchTeams(teamCollection.getTwoYearsItems());
        System.out
                .println("==================Filter By Two Years==========================");
        System.out.println(filter);
        TestCollections collection = new TestCollections(1000000);
        collection.testTime();
    }

}
