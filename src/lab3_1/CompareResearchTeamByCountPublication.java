package lab3_1;

import java.util.Comparator;

public class CompareResearchTeamByCountPublication implements
		Comparator<ResearchTeam> {

	@Override
	public int compare(ResearchTeam resTeam1, ResearchTeam resTeam2) {
		// TODO Auto-generated method stub
		if (resTeam1.getListPublication().size() > resTeam2
				.getListPublication().size())
			return 1;
		else if (resTeam1.getListPublication().size() < resTeam2
				.getListPublication().size())
			return -1;
		return 0;
	}

}
