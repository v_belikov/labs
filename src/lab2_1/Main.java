package lab2_1;

import java.text.ParseException;
import java.util.Random;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws ParseException {
		// TODO Auto-generated method stub

		int rowCount = 0, colCount = 0;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Row Count:");
		rowCount = sc.nextInt();
		System.out.println("Enter Column Count");
		colCount = sc.nextInt();
		Random random = new Random();
		Person[] array = new Person[rowCount * colCount];
		Person[][] matrix = new Person[rowCount][colCount];
		Person[][] jagged = new Person[rowCount][];
		for (int i = 0; i < array.length; i++) {
			int day = random.nextInt(30) + 1;
			int month = random.nextInt(12) + 1;
			int year = random.nextInt(20) + 1980;
			array[i] = new Person("Person " + (i + 1), "ArrayItem", day + "."
					+ (month < 10 ? "0" + month : month) + "." + year);
		}
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				int day = random.nextInt(30) + 1;
				int month = random.nextInt(12) + 1;
				int year = random.nextInt(20) + 1980;
				matrix[i][j] = new Person("Person " + (i + 1), "MatrixItem"
						+ (j + 1), day + "."
						+ (month < 10 ? "0" + month : month) + "." + year);
			}
		}
		int currentCount = rowCount * colCount;
		for (int i = 0; i < jagged.length; i++) {
			int count;
			if (i == jagged.length - 1) {

				count = currentCount;
			} else {
				count = random.nextInt(currentCount / 2) + 1;

			}
			jagged[i] = new Person[count];
			currentCount -= count;
		}
		for (int i = 0; i < jagged.length; i++) {
			for (int j = 0; j < jagged[i].length; j++) {
				int day = random.nextInt(30) + 1;
				int month = random.nextInt(12) + 1;
				int year = random.nextInt(20) + 1980;
				jagged[i][j] = new Person("Person " + (i + 1), "JaggedItem"
						+ (j + 1), day + "."
						+ (month < 10 ? "0" + month : month) + "." + year);
			}
		}
		long currentTime = System.currentTimeMillis();
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}
		System.out.println("Time for array: "
				+ (System.currentTimeMillis() - currentTime));
		currentTime = System.currentTimeMillis();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.println(matrix[i][j]);
			}
		}
		System.out.println("Time for matrix: "
				+ (System.currentTimeMillis() - currentTime));
		currentTime = System.currentTimeMillis();
		for (int i = 0; i < jagged.length; i++) {
			for (int j = 0; j < jagged[i].length; j++) {
				System.out.println(jagged[i][j]);
			}
		}
		System.out.println("Time for jangged: "
				+ (System.currentTimeMillis() - currentTime));
	}
}
