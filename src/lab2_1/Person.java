package lab2_1;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Person implements Serializable {
	private String name;
	private String lastName;
	private Date date;
	private SimpleDateFormat format;

	public Person(String name, String lastName, String date) throws ParseException {
		format = new SimpleDateFormat("dd.MM.yyyy");
		this.setName(name);
		this.setLastName(lastName);
		this.setDate(date);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Date getDate() {
		return date;
	}

	protected boolean setDate(String date) throws ParseException {


			this.date = format.parse(date);

			return true;

	}

	public void setDate(int year) {
		date.setYear(year);
	}

	@Override
	public String toString() {
		return getName() + " " + getLastName() + " " + format.format(getDate());
	}

	public String toShortString() {
		return getName() + " " + getLastName();
	}

	@Override
	public boolean equals(Object object) {
		Person person = (Person) object;
		return getName().equals(person.getName())
				&& getLastName().equals(person.getLastName())
				&& getDate().equals(person.getDate());
	}
	
	@Override
	public int hashCode() {
		String date = format.format(getDate());
		int hash = 0;
		for (int i = 0; i < date.length(); hash += date.charAt(i++))
			;
		for (int i = 0; i < name.length(); hash += name.charAt(i++))
			;
		for (int i = 0; i < lastName.length(); hash += lastName.charAt(i++))
			;

		return hash;
	}
	
	public Object deepCopy() throws ParseException {
		return new Person(new String(getName()), new String(getLastName()),
				format.format(getDate()));
	}
}
